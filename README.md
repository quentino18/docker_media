# Docker media

This docker set up the following : Jackett, Sonarr/Radarr, QBittorrent, FlareSolverr and Emby 
This allows to add followed TV shows to Sonarr and movies to Radarr.
It picks rights torrents from the indexers and the help of FlareSolverr in Jackett and give it to QBittorrent.
When it's finished, it's properly renamed and moved to Zmby.
Emby can also automatically add subtitles of configured.

This is supposed to work behind a proxy (here traefik), but you can remove labels if you don't use it
You have to replace the domain.tld by yours in .env file

#### Create and run media docker as detached
    docker-compose up -d

#### Create and run media docker
    docker-compose up

#### Stop media docker and related network
    docker-compose down

#### Access to logs and follow
    docker-compose logs -f

#### Pull image
    docker-compose pull

#### Open terminal on docker container
    docker containerid exec -it dockname /bin/bash

Please create the following folders : 

Movies are stored in : /data/media/data/movies
Musics are stored in : /data/media/data/musics
Tv shows are stored in : /data/media/data/tv

QBittorrent use the following folder for files in process : /data/media/torrent/downloads
